package
{
    public class Constants
    {
        public static const GameWidth:int  = 768;
        public static const GameHeight:int = 1024;
        
        public static const CenterX:int = GameWidth / 2;
        public static const CenterY:int = GameHeight / 2;
		
		public static const EscalaCores:Array = [
			0x00A1E4, 
			0x00ACC8, 
			0x00B8A5, 
			0x6CC071, 
			0x91C039, 
			0xA6CE39, 
			0xBFCD31, 
			0xD9CF24, 
			0xE9C31D, 
			0xFFD400, 
			0xFDB913, 
			0xFAA61A, 
			0xF68B1F, 
			0xF37021, 
			0xE34A21, 
			0xD81921
		];
    }
}