package events
{
	import starling.events.Event;
	
	public class SceneEvent extends Event
	{
		public static const TRANSITION_IN_START:String = "sceneTransitionInStart";
		public static const TRANSITION_IN_COMPLETE:String = "sceneTransitionInComplete";
		public static const TRANSITION_OUT_START:String = "sceneTransitionOutStart";
		public static const TRANSITION_OUT_COMPLETE:String = "sceneTransitionOutComplete";
		
		public function SceneEvent(type:String, bubbles:Boolean=false, data:Object=null)
		{
			super(type, bubbles, data);
		}
	}
}