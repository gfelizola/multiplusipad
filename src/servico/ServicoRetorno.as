package servico
{
	public class ServicoRetorno
	{
		public var retorno:Object;
		public var sucesso:Boolean = false;
		public var mensagem:String = "";
		
		public function ServicoRetorno(sucesso:Boolean, mensagem:String, retorno:Object)
		{
			this.retorno = retorno;
			this.sucesso = sucesso;
			this.mensagem = mensagem;
		}
		
		/**********************************
		 ** PUBLIC METHODS
		 ***********************************/
		public function toString():String
		{
			return "[object ServicoRetorno]: sucesso: " + this.sucesso + "; mensagem: " + this.mensagem + "; retorno:" + this.retorno ;
		}
		
		/**********************************
		 ** PRIVATE METHODS
		 ***********************************/
		
		/**********************************
		 ** EVENT HANDLERS
		 ***********************************/
		
		/**********************************
		 ** GETTERS / SETTERS
		 ***********************************/
	}
}