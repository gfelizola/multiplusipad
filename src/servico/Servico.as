package servico
{
//	import com.adobe.serialization.json.JSON;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.external.ExternalInterface;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.utils.ByteArray;
	
	public class Servico extends EventDispatcher
	{
		private static const aguardando:String = "aguardando";
		private static const enviando:String = "enviando";
		
		private static var _callbackAtual:Function;
		private static var _fila:Array = [];
		private static var _controle:int = 0;
		private static var _status:String = aguardando;

		private static var _chamadaAtual:Object;
		
		public function Servico()
		{
		}
		
		/**********************************
		 ** PUBLIC METHODS
		 ***********************************/
		
		public static function CopiaArquivosPenDrive(callback:Function = null):void
		{
			var params:Object = {};
			chamaMetodo("CopiaArquivosPenDrive", params, callback);
		}
		
		public static function GravaParticipacaoOnLine(registros:String, ipad:String, callback:Function = null):void
		{
			var dados:Object = {
				pRegistros: registros,
				pIdIpad: ipad
			};
			
			chamaMetodo("GravaParticipacaoOnLine", dados, callback);
		}
		
		public static function GravaParticipacaoOffLine(cpf:String, pontuacao:String, callback:Function = null):void
		{
			var dados:Object = {
				pCPF: cpf,
				pPontuacao: pontuacao
			};
				
			chamaMetodo("GravaParticipacaoOffLine", dados, callback);
		}
		
		/**********************************
		 ** PRIVATE METHODS
		 ***********************************/
		private static function chamaMetodoImagem( acao:String, params:* = null, imagens:* = null, callback:Function = null ):void
		{
			var chamada:Object = {
				acao: acao,
				parametros: params,
				imagens: imagens,
				callback: callback,
				tipo: "imagem",
				controle: _controle++
			};
			
			_fila.push( chamada );
			if( _status == aguardando ){
				proximoDaFila();
			}
		}
		
		private static function chamaMetodo( acao:String, params:* = null, callback:Function = null ):void
		{
			var chamada:Object = {
				acao: acao,
				parametros: params,
				callback: callback,
				tipo: "dados",
				controle: _controle++
			};
			
			_fila.push( chamada );
			if( _status == aguardando ){
				proximoDaFila();
			}
		}
		
		private static function proximoDaFila():void
		{
			var chamada:Object = _fila.shift();
			_chamadaAtual = chamada;
			_callbackAtual = chamada.callback;
			_status = enviando;
				
			trace("Servico.proximoDaFila()", chamada.acao);
			
			if( chamada.tipo == "dados" ){
				
				var urlvars: URLVariables = new URLVariables();
				
				for(var i:String in chamada.parametros) {
					urlvars[i] = chamada.parametros[i];
				}
				
				var urlreq:URLRequest = new URLRequest( Game.UrlServico + "/" + chamada.acao);
				urlreq.method = URLRequestMethod.POST;
				urlreq.data = urlvars;
				
				var loader:URLLoader = new URLLoader();
				loader.addEventListener(Event.COMPLETE, chamadaComplete);
				loader.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
				loader.load(urlreq);
				
			}
		}
		
		private static function onIOError(e:IOErrorEvent):void
		{
			trace( e.errorID, e.text, e.toString() );
			
			_status = aguardando;
			
			if( _callbackAtual is Function ){
				_callbackAtual.call(null, new ServicoRetorno(false, "ERRO: Erro de gravação", null)); 
			}
			
			if ( _fila.length > 0 ) {
				proximoDaFila();
			}
		}
		
		private static function chamadaComplete(e:Event):void
		{
			_status = aguardando;
			trace("Servico.chamadaComplete(e)", _chamadaAtual.acao);
			
			var retXML:XML
			var sr:ServicoRetorno;
			
			if( _chamadaAtual ){
				retXML = new XML( URLLoader(e.target).data );
				sr = new ServicoRetorno( true, "Envio realizado com sucesso", retXML.toString() );
			}
			
			if( _callbackAtual is Function ){
				_callbackAtual.call(null, sr); 
			}
			
			if ( _fila.length > 0 ) {
				proximoDaFila();
			}
		}
		
		/**********************************
		 ** EVENT HANDLERS
		 ***********************************/
		
		/**********************************
		 ** GETTERS / SETTERS
		 ***********************************/
	}
}