package
{
	import flash.desktop.NativeApplication;
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.filesystem.File;
	import flash.geom.Rectangle;
	import flash.system.Capabilities;
	
	import starling.core.Starling;
	import starling.events.Event;
	import starling.utils.AssetManager;
	import starling.utils.RectangleUtil;
	import starling.utils.ScaleMode;
	
	public class MultiplusGameIpad extends Sprite
	{
		private var mStarling:Starling;
		
		public function MultiplusGameIpad()
		{
			var stageWidth:int  = 768;
			var stageHeight:int = 1024;
			var iOS:Boolean = Capabilities.manufacturer.indexOf("iOS") != -1;
			
			Starling.multitouchEnabled = true;  // useful on mobile devices
			Starling.handleLostContext = false;  // not necessary on iOS. Saves a lot of memory!
			
			var viewPort:Rectangle = RectangleUtil.fit(
				new Rectangle(0, 0, stageWidth, stageHeight), 
				new Rectangle(0, 0, stage.fullScreenWidth, stage.fullScreenHeight), 
				ScaleMode.SHOW_ALL, iOS);
			
			var scaleFactor:int = 2;
			var appDir:File = File.applicationDirectory;
			var assets:AssetManager = new AssetManager(scaleFactor);
			
			trace( viewPort );
			
			assets.verbose = Capabilities.isDebugger;
			assets.enqueue( appDir.resolvePath("textures") );
			
			mStarling = new Starling(Game, stage, viewPort);
			mStarling.stage.stageWidth  = stageWidth;
			mStarling.stage.stageHeight = stageHeight;
			mStarling.simulateMultitouch  = false;
			mStarling.enableErrorChecking = false;
			
			mStarling.addEventListener(starling.events.Event.ROOT_CREATED, function():void
			{
				var game:Game = mStarling.root as Game;
				game.start(assets);
				mStarling.start();
			});
			
			// When the game becomes inactive, we pause Starling; otherwise, the enter frame event
			// would report a very long 'passedTime' when the app is reactivated. 
			
			NativeApplication.nativeApplication.addEventListener(
				flash.events.Event.ACTIVATE, function (e:*):void { mStarling.start(); });
			
			NativeApplication.nativeApplication.addEventListener(
				flash.events.Event.DEACTIVATE, function (e:*):void { mStarling.stop(true); });
		}
	}
}