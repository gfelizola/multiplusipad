package 
{
    import flash.display.Stage;
    import flash.system.System;
    import flash.ui.Keyboard;
    
    import avmplus.getQualifiedClassName;
    
    import events.SceneEvent;
    
    import game.Background;
    import game.Easter;
    import game.Participacao;
    
    import nz.co.codec.flexorm.EntityManager;
    
    import scenes.CopiarScene;
    import scenes.HomeScene;
    import scenes.Scene;
    
    import starling.core.Starling;
    import starling.display.Image;
    import starling.display.Sprite;
    import starling.events.Event;
    import starling.events.KeyboardEvent;
    import starling.events.TouchEvent;
    import starling.events.TouchPhase;
    import starling.utils.AssetManager;
    
    import utils.ProgressBar;

    public class Game extends Sprite
    {
		[Embed(source="/fonts/Gotham-Light.otf", embedAsCFF="false", fontFamily="Gotham-Light")]
		private static const GothamLight:Class;
		[Embed(source="/fonts/Gotham-Medium.otf", embedAsCFF="false", fontFamily="Gotham-Medium")]
		private static const GothamMedium:Class;
		
        private static var sAssets:AssetManager;
		
		private static var _urlServico:String;
		private static var _isIpad:Boolean = false;
		
		public var current:Participacao;
		
        private var mLoadingProgress:ProgressBar;
		
        private var _currentScene:Scene;
        private var _container:Sprite;
        private var _nextScene:Class;
		
        private var _logo:Image;
        private var _bg:Background;
		private var _easter:Easter;
		
		private var _scale:Number = 0.6;
        
        public function Game()
        {
            current = new Participacao();
			current.email = "";
			current.cpf = "";
			current.pontuacao = "0";
        }
        
        public function start(assets:AssetManager, ipad:Boolean = false):void
        {
			_isIpad = ipad;
            sAssets = assets;
            
            mLoadingProgress = new ProgressBar(400, 20);
            mLoadingProgress.x = (stage.stageWidth-400) / 2;
            mLoadingProgress.y = 100;
            addChild(mLoadingProgress);
            
            assets.loadQueue(function(ratio:Number):void
            {
                mLoadingProgress.ratio = ratio;
                
                if (ratio == 1)
                    Starling.juggler.delayCall(function():void
                    {
                        mLoadingProgress.removeFromParent(true);
                        mLoadingProgress = null;
                        showMainMenu();
                    }, 0.15);
            });
            
            stage.addEventListener(KeyboardEvent.KEY_DOWN, onKey);
			this.addEventListener(TouchEvent.TOUCH, onTrigger);
        }
		
		private function onTrigger(e:TouchEvent):void
		{
			if( e.getTouch(_logo, TouchPhase.ENDED ) ){
				showScene(HomeScene);
			}
		}
		
        private function showMainMenu():void
        {
            // now would be a good time for a clean-up 
            System.pauseForGCIfCollectionImminent(0);
            System.gc();
			
//			_bg = new Background();
//			addChild(_bg);
			
//			_bg.createBackground();
			
			_logo = new Image( Game.assets.getTexture("logoMultiplus") );
			_logo.x = 48;
			_logo.y = 56;
			addChild(_logo);
			
			_easter = new Easter();
			_easter.addEventListener("ComandoCopiar", onComando);
			addChild(_easter);
			
			var em:EntityManager = EntityManager.instance;
			em.openSyncConnection("multiplus");
			
//			var p:Participacao = new Participacao();
//			p.email = "gfelizola@gmail.com";
//			p.cpf = "123.123.123-00";
//			p.pontuacao = "120";
//			em.save(p);
            
			//iniciar jogo
			showScene(HomeScene);
        }
		
		private function onComando(e:Event):void
		{
			if( e.type == "ComandoFechar"){
				var st:Stage = Starling.current.nativeStage;
				st.nativeWindow.close();
			} else if( e.type == "ComandoMinimizar"){
				var st:Stage = Starling.current.nativeStage;
				st.nativeWindow.minimize();
			} else if( e.type == "ComandoScale"){
				this.scaleX = this.scaleY = _scale;
			} else if( e.type == "ComandoCopiar"){
				this.showScene(CopiarScene);
			}
		}
		
        private function onKey(event:KeyboardEvent):void
        {
            if (event.keyCode == Keyboard.SPACE)
                Starling.current.showStats = !Starling.current.showStats;
            else if (event.keyCode == Keyboard.X)
                Starling.context.dispose();
        }
        
        public function closeScene():void
        {
			_currentScene.addEventListener(SceneEvent.TRANSITION_OUT_COMPLETE, removeScene);
            _currentScene.transitionOut();
        }
		
		public function removeScene(e:Event = null):void
		{
			_currentScene.removeFromParent(true);
			_currentScene = null;
			
			if( _nextScene != null ) showScene(_nextScene);
		}
		
		public function showScene(sceneClass:Class):void
        {
            if (_currentScene){
				_nextScene = sceneClass;
				closeScene();
			} else {
				var mostrar:Boolean = getQualifiedClassName(sceneClass).indexOf("JogoScene") < 0; 
				_logo.visible = mostrar;
//				_bg.visible = mostrar;
					
	            _currentScene = new sceneClass(this) as Scene;
	            addChild(_currentScene);
				_currentScene.transitionIn();
				
				if( _nextScene != null ) _nextScene = null;
			}
            
        }
        
        public static function get assets():AssetManager { return sAssets; }
        public static function get UrlServico():String { 
			if( _isIpad ){
				return "http://multiplus.tribointeractive.com.br/wstotemgamemultiplus.asmx"; 
			} else {
				return "http://www.tribointeractive.com.br:81/multiplus/totemgame/site/wstotemgamemultiplus.asmx" ;
//				return "http://localhost/wstotemgamemultiplus.asmx" ;
			}
		}
    }
}