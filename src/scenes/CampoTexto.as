package scenes
{
	import flash.events.FocusEvent;
	import flash.text.SoftKeyboardType;
	import flash.text.StageText;
	import flash.text.TextFormatAlign;
	
	import starling.display.Sprite;
	
	public class CampoTexto extends Sprite
	{
		private static var _campo:StageText;
		
		private static var _maskCPF:String = "___.___.___-__";
		
		public function CampoTexto()
		{
			super();
		}
		
		public static function criaCPF():StageText
		{
			_campo = new StageText();
			_campo.restrict = "0-9.-";
			_campo.maxChars = 14;
			_campo.color = 0xffffff;
			_campo.fontFamily = "Gotham-Light";
			_campo.fontSize = 68;
			_campo.textAlign = TextFormatAlign.LEFT;
			_campo.softKeyboardType = SoftKeyboardType.NUMBER;
			_campo.editable = true;
			_campo.text = _maskCPF;
			_campo.addEventListener(FocusEvent.FOCUS_IN, onFocus);
			_campo.addEventListener(FocusEvent.FOCUS_OUT, onFocus);
			
			return _campo;
		}
		
		public static function criaEmail():StageText
		{
			_campo = new StageText();
			_campo.color = 0xffffff;
			_campo.fontFamily = "Gotham-Light";
			_campo.fontSize = 42;
			_campo.textAlign = TextFormatAlign.CENTER;
			_campo.softKeyboardType = SoftKeyboardType.EMAIL;
			_campo.editable = true;
			
			return _campo;
		}
		
		private static function onFocus(e:FocusEvent):void
		{
			var txt:StageText = StageText(e.target); 
			if( e.type == FocusEvent.FOCUS_IN ){
				if( txt.text == _maskCPF ) txt.text = ""; 
			} else {
				if( txt.text == "" ) txt.text = _maskCPF;
			}
		}
	}
}