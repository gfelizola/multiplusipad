package scenes
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Linear;
	
	import flash.geom.Point;
	import flash.system.System;
	
	import events.SceneEvent;
	
	import game.Bola;
	
	import org.casalib.util.NumberUtil;
	
	import starling.core.Starling;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	
	public class JogoScene extends Scene
	{
		
		private var parceiros:Vector.<String>;
		private var _bolas:Vector.<Bola>;
		private var _abertas:Vector.<Bola>;
		private var _certas:Vector.<Bola>;
		
		private var _relogio:MovieClip;
		private var _bolasContainer:Sprite;
		
		private var _enable:Boolean = false;
		
		private var twRelogio:TweenMax;
		
		private const TEMPO_JOGADA:Number = 20;
		
		public function JogoScene(g:Game)
		{
			super(g);
		}
		
		override public function create(e:Event=null):void
		{
			System.pauseForGCIfCollectionImminent(0);
			System.gc();
			
			//captura nomes
			var parceirosTemp:Vector.<String> = Game.assets.getTextureNames("parceiro");
			//valida somente 8
			if( parceirosTemp.length > 8 ){
				parceirosTemp.splice(8, parceirosTemp.length - 8 );
			}
			
			//duplica
			parceiros = parceirosTemp.concat(parceirosTemp);
			trace( parceiros );
			
			//randomiza
			parceiros.sort(function(a:Object, b:Object):int { return Math.floor( Math.random() * 3 - 1 ); });
			
			//cria relogio
			_relogio = new MovieClip( Game.assets.getTextureAtlas("relogio").getTextures(), 30);
			_relogio.alignPivot(HAlign.CENTER, VAlign.TOP);
			_relogio.x = stage.stageWidth / 2;
			_relogio.y = 100;
			_relogio.stop();
			addChild(_relogio);
			Starling.juggler.add(_relogio);
			
			//tamanho do stage
			var sw:Number = stage.stageWidth;
			
			_bolasContainer = new Sprite();
			_bolasContainer.x = sw * 0.1;
			_bolasContainer.y = 200;
			addChild( _bolasContainer );
			
			//posicoes das bolas calculadas apartir do tamanho do stage
			var posicoes:Vector.<Point> = new Vector.<Point>();
			posicoes.push( new Point( sw * 0.7, sw * 0.1 ) );
			posicoes.push( new Point( sw * 0.5, sw * 0.1 ) );
			posicoes.push( new Point( sw * 0.3, sw * 0.1 ) );
			posicoes.push( new Point( sw * 0.1, sw * 0.1 ) );
			posicoes.push( new Point( sw * 0.1, sw * 0.3 ) );
			posicoes.push( new Point( sw * 0.1, sw * 0.5 ) );
			posicoes.push( new Point( sw * 0.1, sw * 0.7 ) );
			posicoes.push( new Point( sw * 0.3, sw * 0.7 ) );
			posicoes.push( new Point( sw * 0.5, sw * 0.7 ) );
			posicoes.push( new Point( sw * 0.7, sw * 0.7 ) );
			posicoes.push( new Point( sw * 0.7, sw * 0.5 ) );
			posicoes.push( new Point( sw * 0.7, sw * 0.3 ) );
			posicoes.push( new Point( sw * 0.5, sw * 0.3 ) );
			posicoes.push( new Point( sw * 0.3, sw * 0.3 ) );
			posicoes.push( new Point( sw * 0.3, sw * 0.5 ) );
			posicoes.push( new Point( sw * 0.5, sw * 0.5 ) );
			
			_bolas = new Vector.<Bola>();
			_abertas = new Vector.<Bola>();
			_certas = new Vector.<Bola>();
			
			//criacao das bolinhas (tras e frente );
			var bola:Bola;
			for (var i:int = 0; i < posicoes.length; i++) 
			{
				bola = new Bola();
				//				bola.criar( parceiros[i], Constants.EscalaCores[i], sw * 0.1 );
				bola.criar( parceiros[i], i+1, sw * 0.2 );
				bola.x = posicoes[i].x;
				bola.y = posicoes[i].y;
				
				_bolasContainer.addChild( bola );
				_bolas.push( bola );
				TweenMax.delayedCall(i* 0.01, bola.abrir );
			}
			
			TweenMax.delayedCall( 2, iniciarJogo);
			super.create(e);
		}
		
		private function iniciarJogo():void
		{
			fecharTodasErradas();
			
			this.addEventListener(TouchEvent.TOUCH, onTouch);
			
			var obj:Object = {tempo:0};
			twRelogio = TweenMax.to( obj, TEMPO_JOGADA, {tempo:1000, ease:Linear.easeNone, onUpdate:function():void {
				var frameActual:Number = Math.round( NumberUtil.map( twRelogio._time, 0, twRelogio._totalDuration, 1, _relogio.numFrames ) );
				_relogio.currentFrame = frameActual - 1;
			}, onComplete:fimDeJogo });
		}
		
		private function onTouch(e:TouchEvent):void
		{
			if( ! _enable ) return;
			
			for (var i:int = 0; i < _bolas.length; i++) 
			{
				var b:Bola = _bolas[i];
				var t:Touch = e.getTouch( _bolas[i], TouchPhase.ENDED );
				if( t && b.enabled ){
					b.abrir();
					verificarAbertas(b);
					break;
				}
			}
		}
		
		private function verificarAbertas(b:Bola):void
		{
			_abertas.push( b );
			if( _abertas.length == 2 ){
				_enable = false;
				
				var a:Bola = _abertas[0];
				if( a.parceiro == b.parceiro ){
					_certas.push(a,b);
					
					if( _certas.length == parceiros.length ){
						fimDeJogo();
					} else {
						_enable = true;
					}
				} else {
					TweenMax.delayedCall(1, fecharTodasErradas);
				}
				
				_abertas.length = 0;
			}
		}
		
		private function fimDeJogo():void
		{
			twRelogio.kill();
			
			var placar:Number = _certas.length * 10;
			this.game.current.pontuacao = placar.toString();
			this.game.showScene(FeedbackScene);
		}
		
		private function fecharTodasErradas():void
		{
			for (var i:int = 0; i < _bolas.length; i++) 
			{
				if( _certas.indexOf( _bolas[i] ) < 0 ) {
					_bolas[i].fechar();
				}
			}
			
			_enable = true;
		}
		
		override public function transitionIn(e:Event=null):void
		{
			super.transitionIn(e);
		}
		
		override public function transitionInComplete(e:Event=null):void
		{
			super.transitionInComplete(e);
			
			
		}
		
		override public function transitionOut(e:Event=null):void
		{
			this.removeEventListener(TouchEvent.TOUCH, onTouch);
			
			var chs:Array = [];
			for (var i:int = 0; i < this.numChildren; i++) 
			{
				chs.push( this.getChildAt(i) );
			}
			
			TweenMax.staggerTo(chs, 0.5, {alpha:0}, 0.05, dispatchEventWith, [SceneEvent.TRANSITION_OUT_COMPLETE]);
		}
		
		override public function transitionOutComplete(e:Event=null):void
		{
			super.transitionOutComplete(e);
		}
	}
}