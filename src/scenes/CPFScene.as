package scenes
{
	import com.greensock.TweenMax;
	
	import flash.geom.Rectangle;
	import flash.text.StageText;
	
	import events.SceneEvent;
	
	import starling.core.Starling;
	import starling.display.Button;
	import starling.display.Image;
	import starling.events.Event;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.utils.VAlign;
	
	import utils.Validator;

	public class CPFScene extends Scene
	{
		private var digiteTxt:TextField;
		
		private var inputBG:Image;
		private var btnOk:Button;
		
		private var cpfText:StageText;
		
		private var invalidoTxt:TextField;
		private var porFavorTxt:TextField;
		
		public function CPFScene(g:Game)
		{
			super(g);
		}
		
		override public function create(e:Event = null):void
		{
			digiteTxt = new TextField(600, 150, "DIGITE SEU CPF\nPARA JOGAR", "Gotham-Light", 50, 0x4C4C4C, false);
			digiteTxt.alignPivot("center",VAlign.TOP);
			digiteTxt.x = stage.stageWidth / 2;;
			digiteTxt.y = 260;
			addChild(digiteTxt);
			
			inputBG = new Image( Game.assets.getTexture("inputsBG") );
			inputBG.alignPivot("center",VAlign.TOP);
			inputBG.x = stage.stageWidth / 2;
			inputBG.y = 460;
			addChild(inputBG);
			
			cpfText = CampoTexto.criaCPF();
			cpfText.stage = Starling.current.nativeStage;
			cpfText.viewPort = new Rectangle( (( inputBG.x - (inputBG.width / 2) ) * 2) + 100, (inputBG.y * 2) + 40, (inputBG.width * 2) - 200, (inputBG.height * 2) - 80);
			cpfText.assignFocus();
			
			invalidoTxt = new TextField(inputBG.width, 60, "CPF inválido", "Gotham-Light", 28, 0xFFFFFF, false);
			invalidoTxt.alignPivot("center",VAlign.TOP);
			invalidoTxt.x = inputBG.x
			invalidoTxt.y = inputBG.y + 16;
			invalidoTxt.visible = false;
			addChild(invalidoTxt);
			
			porFavorTxt = new TextField(inputBG.width, 60, "por favor, digite novamente", "Gotham-Light", 18, 0xFFFFFF, false);
			porFavorTxt.alignPivot("center",VAlign.TOP);
			porFavorTxt.x = inputBG.x;
			porFavorTxt.y = inputBG.y + 40;
			porFavorTxt.visible = false;
			addChild(porFavorTxt);
			
			btnOk = new Button(Game.assets.getTexture("btnOkBG"), "OK");
			btnOk.fontName = "Gotham-Light";
			btnOk.fontSize = 34;
			btnOk.fontColor = 0xFFFFFF;
			btnOk.alignPivot("center",VAlign.TOP);
			btnOk.x = stage.stageWidth / 2;
			btnOk.y = 630;
			addChild(btnOk);
			
			this.addEventListener(Event.TRIGGERED, onBtTrigger);
		}
		
		private function onBtTrigger(e:Event):void
		{
			var bt:Button = e.target as Button;
			if( bt == btnOk ){
				if( btnOk.text == "<" ){
					mostraCampo();
				} else {
					if( Validator.validateCPF(cpfText.text)){
						this.game.current.cpf = cpfText.text;
						this.game.showScene(EmailScene);
					} else {
						mostraInvalido();
					}
				}
			}
		}
		
		private function mostraInvalido():void
		{
			cpfText.visible = false;
			invalidoTxt.visible = true;
			porFavorTxt.visible = true;
			btnOk.text = "<";
		}
		
		private function mostraCampo():void
		{
			cpfText.visible = true;
			invalidoTxt.visible = false;
			porFavorTxt.visible = false;
			btnOk.text = "OK";
		}
		
		private function onTouch(e:TouchEvent):void
		{
			if( invalidoTxt.visible ){
				if( 
					e.getTouch( inputBG, TouchPhase.ENDED ) ||
					e.getTouch( invalidoTxt, TouchPhase.ENDED ) ||
					e.getTouch( porFavorTxt, TouchPhase.ENDED ) ){
					mostraCampo();
				}
			}
		}
		
		override public function transitionIn(e:Event=null):void
		{
			TweenMax.staggerFrom([ digiteTxt, inputBG, btnOk ], 1, {alpha:0}, 0.5, dispatchEventWith, [SceneEvent.TRANSITION_IN_COMPLETE]);
		}
		
		override public function transitionOut(e:Event=null):void
		{
			this.removeEventListener(Event.TRIGGERED, onBtTrigger);
			cpfText.visible = false;
			cpfText.dispose();
			
			invalidoTxt.visible = false;
			porFavorTxt.visible = false;
			
			TweenMax.staggerTo([ btnOk, inputBG, digiteTxt], 0.5, {alpha:0}, 0.1, dispatchEventWith, [SceneEvent.TRANSITION_OUT_COMPLETE]);
		}
	}
}