package scenes
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Linear;
	
	import flash.utils.Dictionary;
	
	import mx.collections.ArrayCollection;
	
	import game.Participacao;
	
	import nz.co.codec.flexorm.EntityManager;
	
	import pl.mateuszmackowiak.nativeANE.properties.SystemProperties;
	
	import servico.Servico;
	import servico.ServicoRetorno;
	
	import starling.display.Button;
	import starling.events.Event;
	import starling.utils.VAlign;
	
	import utils.ProgressBar;

	public class CopiarScene extends Scene
	{
		private var btnIncio:Button;
		private var btnVoltar:Button;
		
		private var _progress:ProgressBar;
		
		
		public function CopiarScene(g:Game)
		{
			super(g);
		}
		
		override public function create(e:Event=null):void
		{
			btnIncio = new Button( Game.assets.getTexture("btnInicioBG"), "INICIAR");
			btnIncio.fontName = "Gotham-Light";
			btnIncio.fontSize = 40;
			btnIncio.fontColor = 0xFFFFFF;
			btnIncio.alignPivot("center",VAlign.TOP);
			btnIncio.x = stage.stageWidth / 2;
			btnIncio.y = stage.stageHeight / 2;
			addChild(btnIncio);
			
			btnVoltar = new Button( Game.assets.getTexture("btnInicioBG"), "VOLTAR");
			btnVoltar.fontName = "Gotham-Light";
			btnVoltar.fontSize = 40;
			btnVoltar.fontColor = 0xFFFFFF;
			btnVoltar.alignPivot("center",VAlign.TOP);
			btnVoltar.x = stage.stageWidth / 2;
			btnVoltar.y = btnIncio.y + btnIncio.height + 20;
			addChild(btnVoltar);
			
			_progress = new ProgressBar(400, 20);
			_progress.x = (stage.stageWidth-400) / 2;
			_progress.y = 400;
			_progress.ratio = 0;
			addChild(_progress);
			
			this.addEventListener(Event.TRIGGERED, onTrigger);
			
			super.create(e);
		}
		
		private function onTrigger(e:Event):void
		{
			if( e.target == btnIncio ){
				iniciarCopia();
			} else if( e.target == btnVoltar ){
				btnIncio.enabled = false;
				btnVoltar.enabled = false;
				
				btnIncio.alpha = 0.5;
				btnVoltar.alpha = 0.5;
				
				this.game.showScene(HomeScene);
			}
		}
		
		private function iniciarCopia():void
		{
			btnIncio.enabled = false;
			btnVoltar.enabled = false;
			
			btnIncio.alpha = 0.5;
			btnVoltar.alpha = 0.5;
			
			var obj:Object = { ratio:0 };
			TweenMax.to( obj, 10, { ratio:1, ease:Linear.easeNone, onUpdate:function():void{
				_progress.ratio = obj.ratio;
			}});
			
			var em:EntityManager = EntityManager.instance;
			var participacoes:ArrayCollection = em.findAll(Participacao);
			var todas:String = "";
			
			for (var i:int = 0; i < participacoes.length; i++) 
			{
				var p:Participacao = participacoes[i];
				todas += p.toString();
			}
			
			trace( todas );
			
			var udid:String = "";
			var nome:String = "";
			
			if( SystemProperties.isSupported ) {
				var dictionary:Dictionary = SystemProperties.getInstance().getSystemProperites();
				
				if(dictionary){
					nome = dictionary["name"]; 
					udid = dictionary["UDID"]; 
				}
				
				dictionary = null;
			}
			
			Servico.GravaParticipacaoOnLine(todas, nome, onCopiou);
		}
		
		private function onCopiou(sr:ServicoRetorno = null):void
		{
			trace(sr);
			
			TweenMax.killAll();
			
			btnIncio.enabled = true;
			btnVoltar.enabled = true;
			
			btnIncio.alpha = 1;
			btnVoltar.alpha = 1;
			_progress.ratio = 1;
		}
		
		override public function transitionIn(e:Event=null):void
		{
			super.transitionIn(e);
		}
		
		override public function transitionInComplete(e:Event=null):void
		{
			// TODO Auto Generated method stub
			super.transitionInComplete(e);
		}
		
		override public function transitionOut(e:Event=null):void
		{
			// TODO Auto Generated method stub
			super.transitionOut(e);
		}
		
		override public function transitionOutComplete(e:Event=null):void
		{
			// TODO Auto Generated method stub
			super.transitionOutComplete(e);
		}
		
		
	}
}