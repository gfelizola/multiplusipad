package scenes
{
	import com.greensock.TweenMax;
	
	import flash.net.NetworkInfo;
	import flash.net.NetworkInterface;
	import flash.utils.Dictionary;
	
	import events.SceneEvent;
	
	import game.ParceiroHome;
	import game.Participacao;
	
	import pl.mateuszmackowiak.nativeANE.properties.SystemProperties;
	
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.TextField;
	import starling.utils.HAlign;
	import starling.utils.VAlign;

	public class HomeScene extends Scene
	{
		private var toqueImg:Image;
		private var toqueTxt:TextField;
		
		private var comeceTxt:TextField;
		private var btnIncio:Button;
		
		private var parceiros:Vector.<String>;
		private var currentParceiro:int = 0;

		private var parceiro:Sprite;
		
		public function HomeScene(g:Game)
		{
			super(g);
		}
		
		override public function create(e:Event = null):void
		{
			super.create(e);
			
			comeceTxt = new TextField(600, 300, "COMECE A\nGANHAR PONTOS\nAGORA MESMO", "Gotham-Light", 50, 0x4C4C4C, false);
			comeceTxt.alignPivot("center",VAlign.TOP);
			comeceTxt.x = stage.stageWidth / 2;
			comeceTxt.y = 200;
			comeceTxt.hAlign = "center";
			addChild(comeceTxt);
			
			btnIncio = new Button( Game.assets.getTexture("btnInicioBG"), "INICIAR");
			btnIncio.fontName = "Gotham-Light";
			btnIncio.fontSize = 40;
			btnIncio.fontColor = 0xFFFFFF;
			btnIncio.alignPivot("center",VAlign.TOP);
			btnIncio.x = stage.stageWidth / 2;
			btnIncio.y = 500;
			addChild(btnIncio);
			
			parceiro = new ParceiroHome();
			parceiro.alignPivot(HAlign.CENTER, VAlign.TOP);
			parceiro.x = stage.stageWidth / 2;
			parceiro.y = 700;
			this.addChild(parceiro);
			
			this.game.current = new Participacao();
			
			if (NetworkInfo.isSupported) { 
				trace("network information is supported");
				
				var network:NetworkInfo = NetworkInfo.networkInfo; 
				for each (var object:NetworkInterface in network.findInterfaces()) { 
					if (object.hardwareAddress) { 
						trace(object.hardwareAddress); 
					} 
				}
			} else {
				trace("Sem network info");
			}
			
			var udid:String = "";
			var nome:String = "";
			
			if( SystemProperties.isSupported ) {
				var dictionary:Dictionary = SystemProperties.getInstance().getSystemProperites();
				
				if(dictionary){
					nome = dictionary["name"]; 
					udid = dictionary["UDID"]; 
				}
				
				dictionary = null;
			}
			
			trace( "nome:", nome);
			trace( "UDID:", udid);
		}
		
		override public function transitionIn(e:Event=null):void
		{
			TweenMax.staggerFrom([ comeceTxt, btnIncio, parceiro], 0.3, {alpha:0}, 0.1, dispatchEventWith, [SceneEvent.TRANSITION_IN_COMPLETE]);
		}
		
		override public function transitionInComplete(e:Event=null):void
		{
			this.addEventListener(Event.TRIGGERED, onBtTrigger);
		}
		
		override public function transitionOutComplete(e:Event=null):void
		{
			this.removeEventListener(Event.TRIGGERED, onBtTrigger);
			
			var chs:Array = [];
			for (var i:int = 0; i < this.numChildren; i++) 
			{
				chs.push( this.getChildAt(i) );
			}
			
			super.transitionOutComplete(e);
		}
		
		override public function transitionOut(e:Event=null):void
		{
			TweenMax.killTweensOf(parceiro);
			TweenMax.staggerTo([ comeceTxt, btnIncio, parceiro], 0.3, {alpha:0}, 0.1, dispatchEventWith, [SceneEvent.TRANSITION_OUT_COMPLETE]);
		}
		
		private function onBtTrigger(e:Event):void
		{
			var bt:Button = e.target as Button;
			if( bt == btnIncio ){
				btnIncio.enabled = false;
				btnIncio.alpha = 0.5;
				this.game.showScene(CPFScene);
			}
		}
	}
}

