package scenes
{
	
	import com.greensock.TweenMax;
	
	import flash.display.BitmapData;
	import flash.display.Shape;
	
	import events.SceneEvent;
	
	import game.Participacao;
	
	import nz.co.codec.flexorm.EntityManager;
	
	import starling.display.Button;
	import starling.display.Image;
	import starling.events.Event;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.HAlign;
	import starling.utils.VAlign;

	public class FeedbackScene extends Scene
	{
		private var parabens:TextField;
		private var ganhou:TextField;
		private var placar:TextField;
		private var pontos:TextField;
		private var multiplus:TextField;
		
		private var btnIncio:Button;
		
		private var linha1:Image;
		private var linha2:Image;
		private var linha3:Image;
		
		public function FeedbackScene(g:Game)
		{
			super(g);
		}
		
		override public function create(e:Event=null):void
		{
			super.create(e);
			
			parabens = new TextField(500, 80, "PARABÉNS!", "Gotham-Light", 66, 0x4C4C4C, false);
			parabens.hAlign = HAlign.CENTER;
			parabens.vAlign = VAlign.TOP;
			parabens.alignPivot(HAlign.CENTER, VAlign.TOP);
			parabens.x = stage.stageWidth / 2;
			parabens.y = 220;
			addChild(parabens);
			
			ganhou = new TextField(500, 60, "Você ganhou", "Gotham-Light", 47, 0x4C4C4C, false);
			ganhou.hAlign = HAlign.CENTER;
			ganhou.vAlign = VAlign.TOP;
			ganhou.alignPivot(HAlign.CENTER, VAlign.TOP);
			ganhou.x = stage.stageWidth / 2;
			ganhou.y = 300;
			addChild(ganhou);
			
			var pontosStr:String = this.game.current.pontuacao;
			while( pontosStr.length < 3 ) pontosStr = "0" + pontosStr;
			
			placar = new TextField(600, 300, this.game.current.pontuacao, "Gotham-Medium", 230, 0x4C4C4C, false);
			placar.hAlign = HAlign.CENTER;
			placar.vAlign = VAlign.TOP;
			placar.alignPivot(HAlign.CENTER, VAlign.TOP);
			placar.x = stage.stageWidth / 2;
			placar.y = 320;
			addChild(placar);
			
			pontos = new TextField(500, 120, "PONTOS", "Gotham-Light", 90, 0x4C4C4C, false);
			pontos.hAlign = HAlign.CENTER;
			pontos.vAlign = VAlign.TOP;
			pontos.alignPivot(HAlign.CENTER, VAlign.TOP);
			pontos.x = stage.stageWidth / 2;
			pontos.y = 555;
			addChild(pontos);
			
			multiplus = new TextField(500, 80, "MULTIPLUS", "Gotham-Light", 68, 0x4C4C4C, false);
			multiplus.hAlign = HAlign.CENTER;
			multiplus.vAlign = VAlign.TOP;
			multiplus.alignPivot(HAlign.CENTER, VAlign.TOP);
			multiplus.x = stage.stageWidth / 2;
			multiplus.y = 650;
			addChild(multiplus);
			
			linha1 = new Image(desenhaLinha(28));
			linha1.x = 190;
			linha1.y = 334;
			addChild(linha1);
			
			linha2 = new Image(desenhaLinha(28));
			linha2.x = 545;
			linha2.y = 334;
			addChild(linha2);
			
			linha3 = new Image(desenhaLinha(382));
			linha3.x = 190;
			linha3.y = 730;
			addChild(linha3);
			
			btnIncio = new Button( Game.assets.getTexture("btnInicioBG"), "INÍCIO");
			btnIncio.fontName = "Gotham-Light";
			btnIncio.fontSize = 40;
			btnIncio.fontColor = 0xFFFFFF;
			btnIncio.alignPivot("center",VAlign.TOP);
			btnIncio.x = stage.stageWidth / 2;
			btnIncio.y = 800;
			addChild(btnIncio);
			
			this.addEventListener(Event.TRIGGERED, onBtTrigger);
			
			//envia dados para banco
			var em:EntityManager = EntityManager.instance;
			em.openSyncConnection("multiplus");
			
			var p:Participacao = this.game.current;
			trace( "Salvando participacao", p);
			em.save(p);
			
//			Servico.GravaParticipacaoOffLine( this.game.current.CPF, this.game.current.Pontos, onGravou );
		}
		
		private function desenhaLinha(w:int):Texture
		{
			var s:Shape = new Shape();
			s.graphics.lineStyle(2, 0x4C4C4C);
			s.graphics.lineTo(w,0);
			
			var bmd:BitmapData = new BitmapData(w, 2, false, 0x4C4C4C);
			bmd.draw(s);
			
			return Texture.fromBitmapData(bmd);
		}
		
		private function onBtTrigger(e:Event):void
		{
			var bt:Button = e.target as Button;
			if( bt == btnIncio ){
				btnIncio.enabled = false;
				btnIncio.alpha = 0.5;
				this.game.showScene(HomeScene);
			}
		}
		
		override public function transitionIn(e:Event=null):void
		{
			var chs:Array = [];
			for (var i:int = 0; i < this.numChildren; i++) 
			{
				chs.push( this.getChildAt(i) );
			}
			
			TweenMax.staggerFrom(chs, 0.5, {alpha:0}, 0.2, dispatchEventWith, [SceneEvent.TRANSITION_IN_COMPLETE]);
		}
		
		override public function transitionOut(e:Event=null):void
		{
			this.removeEventListener(Event.TRIGGERED, onBtTrigger);
			
			var chs:Array = [];
			for (var i:int = 0; i < this.numChildren; i++) 
			{
				chs.push( this.getChildAt(i) );
			}
			
			TweenMax.staggerTo(chs, 0.5, {alpha:0}, 0.05, dispatchEventWith, [SceneEvent.TRANSITION_OUT_COMPLETE]);
		}
		
		override public function transitionOutComplete(e:Event=null):void
		{
			while(this.numChildren){
				this.removeChildAt(0, true);
			}
			
			super.transitionOutComplete(e);
		}
		
		
	}
}