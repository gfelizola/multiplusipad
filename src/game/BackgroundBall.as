package game
{
	import flash.display.BitmapData;
	import flash.display.Shape;
	
	import org.casalib.util.NumberUtil;
	
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.filters.BlurFilter;
	import starling.textures.Texture;
	
	public class BackgroundBall extends Sprite
	{
		public var bola:Image;
		
		public function BackgroundBall()
		{
			super();
		}
		
		public function create(cor:uint):void
		{
			var size:Number = 260;
			var shape:Shape = new Shape();
			shape.graphics.beginFill(cor,1);
			shape.graphics.drawCircle(size/2, size/2, size/2);
			shape.graphics.endFill();
			
			var bmd:BitmapData = new BitmapData(size + 20, size + 20, true, cor);
			bmd.draw(shape);
			
			var txt:Texture = Texture.fromBitmapData(bmd);
			bola = new Image(txt);
			
			var scale:Number = NumberUtil.randomWithinRange(0.6, 1);
			bola.x = NumberUtil.randomWithinRange(-400, 400);
			bola.y = NumberUtil.randomWithinRange(Constants.GameHeight + 260, Constants.GameHeight + 400);
			bola.scaleX = bola.scaleY = scale;
			bola.alpha = NumberUtil.randomWithinRange(0.6, 1);
			
			var blurAmount:Number = NumberUtil.randomIntegerWithinRange(-5, 10);
			if( blurAmount < 0 ) blurAmount = 0;
			
			var f:BlurFilter = new BlurFilter(blurAmount, blurAmount);
			bola.filter = f;
			
			addChild(bola);
		}
		
		public function animar(d:Number = 0):void
		{
			var tween:Tween = new Tween(bola, 20);
			tween.moveTo( NumberUtil.randomWithinRange( Constants.GameWidth - 400, Constants.GameWidth + 400), -300 );
			tween.scaleTo( NumberUtil.randomWithinRange(0.5,1) );
			tween.delay = d;
			tween.repeatCount = 0;
			tween.repeatDelay = 80;
			
			Starling.juggler.add(tween);
		}
	}
}