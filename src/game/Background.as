package game
{
	import starling.display.Sprite;
	
	public class Background extends Sprite
	{
		private var bolas:Array = [];
		private var colorScale1:int = 0;
		private var colorScale2:int = 0;
		
		public function Background()
		{
			super();
		}
		
		public function createBackground():void
		{
			var cores:Array = Constants.EscalaCores;
			var d:Number = 0;
			for (var j:int = 0; j < cores.length; j++) 
			{
				for (var k:int = 0; k < 3; k++) 
				{
					var ball:BackgroundBall = new BackgroundBall();
					ball.create( cores[j] );
					ball.animar( d += 2 );
					
					addChild(ball);
				}
			}
		}
	}
}