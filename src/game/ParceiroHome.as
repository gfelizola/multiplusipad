package game
{
	import starling.core.Starling;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	
	public class ParceiroHome extends Sprite
	{
		
		private var currentParceiro:int = -1;
		private var parceiros:Vector.<String>;
		
		public function ParceiroHome()
		{
			super();
			
			if( stage ) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
			
		}
		
		private function init(e:Event = null):void
		{
			parceiros = Game.assets.getTextureNames("marca");
			parceiros.sort(function(a:Object, b:Object):int {
				return Math.floor( Math.random() * 3 - 1 );
			});
			
			mostrarParceiro();
		}
		
		private function mostrarParceiro():void
		{
			currentParceiro++;
			if( currentParceiro >= parceiros.length ){
				currentParceiro = 0;
			}
			
			var bola:MovieClip = new MovieClip( Game.assets.getTextureAtlas( parceiros[ currentParceiro ] ).getTextures(), 30 );
			bola.alignPivot(HAlign.CENTER, VAlign.TOP);
			bola.x = 0;
			bola.loop = false;
			bola.addEventListener(Event.COMPLETE, onBolaComplete);
			Starling.juggler.add( bola );
			
			this.addChild(bola);
		}
		
		private function onBolaComplete(e:Event):void
		{
			var bola:MovieClip = e.target as MovieClip;
			Starling.juggler.remove(bola);
			bola.removeFromParent(true);
			bola = null;
			mostrarParceiro();
		}
	}
}