package game
{
	import com.greensock.TweenMax;
	
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	
	public class Easter extends Sprite
	{
		private var bt1:Quad;
		private var bt2:Quad;
		private var bt3:Quad;
		
		private var sqAtual:Array = [];
		private var sqFechar:String = "1321";
		private var sqMinim:String = "1232";
		private var sqScale:String = "3121";
		private var sqCopiar:String = "2213";
		
		public function Easter()
		{
			super();
			
			if(stage) start();
			else addEventListener(Event.ADDED_TO_STAGE, start);
		}
		
		private function start(e:Event = null):void
		{
			bt1 = new Quad(stage.stageWidth/3, 50, 0xff0000);
			bt2 = new Quad(stage.stageWidth/3, 50, 0xff0000);
			bt3 = new Quad(stage.stageWidth/3, 50, 0xff0000);
			
			bt1.x = 0;
			bt2.x = stage.stageWidth * 0.33;
			bt3.x = stage.stageWidth * 0.67;
			
			bt1.alpha = bt2.alpha = bt3.alpha = 0;
			
			addChild(bt1);
			addChild(bt2);
			addChild(bt3);
			
			this.addEventListener(TouchEvent.TOUCH, onTrigger);
		}
		
		private function onTrigger(e:TouchEvent):void
		{
//			var q:Quad = e.target as Quad;
//			trace("easter trigger");
			var t1:Touch = e.getTouch(bt1, TouchPhase.ENDED);
			var t2:Touch = e.getTouch(bt2, TouchPhase.ENDED);
			var t3:Touch = e.getTouch(bt3, TouchPhase.ENDED);
			
			if(t1){
				sqAtual.push("1");
				animaTouch(bt1);
				verificaSequencia();
			} else if(t2){
				sqAtual.push("2");
				animaTouch(bt2);
				verificaSequencia();
			} else if(t3){
				sqAtual.push("3");
				animaTouch(bt3);
				verificaSequencia();
			}
		}
		
		private function animaTouch(target:Quad):void
		{
			TweenMax.fromTo(target, 0.1, {alpha:0.3}, {alpha:0});
		}
		
		private function verificaSequencia():void
		{
			if( sqAtual.length >= 4 ){
				var sq:String = sqAtual.join("");
				sq = sq.substr(sq.length-4);
				trace( sq );
//				sqAtual.length = 0;
				
				if( sq == sqFechar ){
					dispatchEventWith("ComandoFechar", false);
				} else if( sq == sqMinim ){
					dispatchEventWith("ComandoMinimizar", false);
				} else if( sq == sqScale ){
					dispatchEventWith("ComandoScale", false);
				} else if( sq == sqCopiar ){
					dispatchEventWith("ComandoCopiar", false);
				}
			}
		}
	}
}