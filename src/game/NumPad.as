package game
{
	import starling.display.Button;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.TouchEvent;
	
	public class NumPad extends Sprite
	{
		public static const BUTTON_PRESS:String = "numPadButtonPress";
		
		public function NumPad()
		{
			super();
			
			var chars:String = "123456789X0<";
			var posX:Number = 0;
			var posY:Number = 0;
			for (var i:int = 0; i < chars.length; i++) 
			{
				var c:String = chars.charAt(i);
				var b:Button = new Button(Game.assets.getTexture("keyBG"), c);
				b.x = posX;
				b.y = posY;
				b.name = "bt-" + c;
				
				b.fontName = "Gotham-Light";
				b.fontSize = 40;
				b.fontColor = 0x4C4C4C;
				
				addChild(b);
				
				posX += b.width - 1;
				
				if((i % 3) == 2){
					posX = 0;
					posY += b.height - 2;
				}
			}
			
			this.addEventListener(Event.TRIGGERED, onButtonTrigger);
			
		}
		
		private function onButtonTrigger(e:Event):void
		{
			var b:Button = e.target as Button;
			dispatchEventWith(NumPad.BUTTON_PRESS, false, b.name.substr(3) );
		}
	}
}