package game
{
	[Bindable]
	public class Participacao
	{
		public var id:int;
		public var cpf:String;
		public var email:String;
		public var pontuacao:String;
		
		public function Participacao()
		{
		}
		
		public function toString():String
		{
			return cpf + "," + email + "," + pontuacao + ";";
		}
	}
}